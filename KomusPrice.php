<?php

namespace Firstbit;

use Bitrix\Catalog\Model\Price;
use Bitrix\Catalog\Model\Product;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\DB\Exception;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\FileTable;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectNotFoundException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SystemException;

class KomusPrice
{

    private const
        IBLOCK_CODE = 'products',
        TABLE_CATEGORIES = 'komusprice_categories',
        TABLE_OFFERS = 'komusprice_offers',
        TABLE_PARAMSLIST = 'komusprice_paramslist',
        MAX_SECTION_DEPTH = 10,
        PHOTOGALLERY_NAME = 'Фотогалерея',
        PHOTOGALLERY_CODE = 'PHOTOGALLERY',
        LOG_LEVELS = ['NONE', 'FATAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'];

    private
        $logFile,
        $zipFileName,
        $xmlFileName,
        $uploadDir,
        $XML,
        $paramsList,
        $iblockId,
        $photogalleryPropertyId,
        $logLevel = 4;

    public function __construct()
    {
        if (!isset($_SERVER['DOCUMENT_ROOT'])) {
            $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__, 5);
        }

        $this->logFile = $_SERVER['DOCUMENT_ROOT'] . '/upload/import/komus-opt/import.log'; // TODO ротация лога
        $this->uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/upload/import/komus-opt';
        $this->imagesTmpDir = $this->uploadDir . '/images';

        $iblock = IblockTable::getList([
            'select' => ['ID'],
            'filter' => ['CODE' => self::IBLOCK_CODE],
            'limit' => 1,
        ])->fetch();

        if ((int)$iblock['ID'] === 0) {
            throw new \Exception('Не удалось определить ID инфоблока с товарами. Нужно создать инфоблок с кодом products');
        }

        $this->iblockId = (int)$iblock['ID'];
    }

    public function __destruct()
    {
        @unlink($this->uploadDir . '/' . $this->xmlFileName);
    }


    public function setLogLevel($level): void
    {
        if (is_int($level) && $level >= 0 && $level <= (count(self::LOG_LEVELS) - 1)) {
            $this->logLevel = $level;
        }

        if (is_string($level) && in_array($level, self::LOG_LEVELS)) {
            $this->logLevel = array_search($level, self::LOG_LEVELS, true);
        }

        $this->addLogMessage('Установлен уровень журналирования ' . self::LOG_LEVELS[ $this->logLevel ]);
    }


    public function addLogMessage($message = '', $level = 'INFO'): void
    {
        if (is_int($level) && $level >= 0 && $level <= (count(self::LOG_LEVELS) - 1)) {
            $level = self::LOG_LEVELS[ $level ];
        }

        if (empty($level) || !in_array($level, self::LOG_LEVELS, true) || array_search($level, self::LOG_LEVELS, true) > $this->logLevel) {
            return;
        }

        $time = date_create_from_format('U.u', number_format(microtime(true), 6, '.', ''))->setTimezone(new \DateTimeZone('Europe/Moscow'))->format('Y-m-d H:i:s.u');

        $memoryBytes = memory_get_usage();
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        $memory = number_format(@round($memoryBytes/(1024 ** ($i = floor(log($memoryBytes, 1024)))),2), 2).' '.$unit[$i];

        file_put_contents($this->logFile, $time . "\t" . $memory . "\t" . $level . "\t" . $message . chr(10), FILE_APPEND);
    }


    /**
     * Получение имени загруженного zip-архива
     *
     * @throws \Exception
     */
    public function getZipFile(): void
    {
        $arFilesList = scandir($this->uploadDir);
        foreach ($arFilesList as $fileName) {
            $ext = substr($fileName, -3, 3);
            if ($ext !== 'zip') {
                continue;
            }
            $this->zipFileName = $fileName;
            break; // TODO Возможно, нужно не прерываться на первом же архиве, а обработать все
        }

        if (!isset($this->zipFileName)) {
            throw new \Exception('В каталоге [' . $this->uploadDir . '] не найден zip-архив с прайсом, выполнение прервано');
        }
        $this->addLogMessage('Найден архив ' . $this->zipFileName . ' с датой ' . date('Y-m-d H:i:s', filemtime($this->uploadDir . '/' . $this->zipFileName)));
    }


    /**
     * Разворот zip-архива и перемещение его в /upload/import/komus-opt/archive/
     *
     * @throws \Exception
     */
    public function extractZipFile(): void
    {
        if (!isset($this->zipFileName)) {
            throw new \Exception('В каталоге [' . $this->uploadDir . '] не найден zip-архив с прайсом');
        }

        try {
            $zip = new \ZipArchive();
            $zip->open($this->uploadDir . '/' . $this->zipFileName);

            for ($i = 0; $i < $zip->numFiles; $i++) {
                $fileName = $zip->getNameIndex($i);
                $ext = substr($fileName, -3, 3);
                if ($ext === 'xml') {
                    $this->xmlFileName = $fileName;
                    break;  // TODO Возможно, нужно не прерываться на первом же xml-файле, а обработать все
                }
            }

            if (!isset($this->xmlFileName)) {
                throw new \Exception('В архиве [' . $this->zipFileName . '] не найден xml с прайсом');
            }

            $this->addLogMessage('Будет обрабатываться файл [' . $this->xmlFileName . ']');

            $zip->extractTo($this->uploadDir);
            $zip->close();

            rename(
                $this->uploadDir . '/' . $this->zipFileName,
                $this->uploadDir . '/archive/' . date('Y-m-d') . '_yml_price_moscow.zip'
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * Проверка XML-файла: количество категорий и предложений
     *
     * @throws \Exception
     */
    public function checkXMLFile(): void
    {
        $xmlFile = $this->uploadDir . '/' . $this->xmlFileName;

        libxml_use_internal_errors(true);
        $objXML = simplexml_load_file($xmlFile);
        if (!$objXML) {
            $errMsg = '';
            foreach (libxml_get_errors() as $err) {
                $errMsg .= $err->message . ' в строке ' . $err->line . '; ';
            }
            throw new \Exception('Ошибка разбора XML из файла [' . $xmlFile . ']: [' . $errMsg . ']');
        }

        $tmp = json_decode(json_encode($objXML->shop->name), true);
        $this->addLogMessage('Наименование прайс-листа: ' . $tmp[0]);

        $tmp = json_decode(json_encode($objXML->attributes()), true);
        $this->addLogMessage('Дата прайс-листа: ' . $tmp['@attributes']['date']);

        $this->addLogMessage('Подсчет количества категорий...');
        $categoriesCount = (int)$objXML->shop->categories->category->count();
        if ($categoriesCount === 0) {
            throw new \Exception('В xml-файле не найден раздел shop->categories');
        }
        $this->addLogMessage('Найдено категорий: ' . $categoriesCount);

        $this->addLogMessage('Подсчет количества предложений...');
        $offersCount = (int)$objXML->shop->offers->offer->count();
        if ($categoriesCount === 0) {
            throw new \Exception('В xml-файле не найден раздел shop->offers');
        }
        $this->addLogMessage('Найдено предложений: ' . $offersCount);

        $this->XML = $objXML;
    }


    /**
     * Чтение списка категорий (разделов) из XML и внесение их во временную таблицу для последующего обновления разделов инфоблока с товарами
     *
     * @throws \Exception
     */
    public function getCategoriesToTemporaryTable(): void
    {
        if (get_class($this->XML) !== 'SimpleXMLElement') {
            throw new \Exception('Прайс-лист не является объектом SimpleXMLElement');
        }

        $this->addLogMessage('Запись категорий во временную таблицу');
        $arCategories = [];
        foreach ($this->XML->shop->categories->category as $category) {
            $attr = json_decode(json_encode($category->attributes()), true);
            $name = json_decode(json_encode($category), true);

            $arCategories[] = [
                'id' => (int)$attr['@attributes']['id'],
                'parentId' => (int)$attr['@attributes']['parentId'],
                'name' => trim($name[0])
            ];
        }

        $connection = Application::getConnection();
        try {
            $connection->query('DROP TABLE IF EXISTS `' . self::TABLE_CATEGORIES . '`');
            $sql = 'CREATE TABLE `' . self::TABLE_CATEGORIES . '` (
                    `ID` int(10) unsigned NOT NULL,
                    `PARENT_ID` int(10) unsigned DEFAULT NULL,
                    `BITRIX_ID` int(10) unsigned DEFAULT NULL,
                    `NAME` text COLLATE utf8_unicode_ci,
                    PRIMARY KEY (`ID`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
            $connection->query($sql);

            foreach ($arCategories as $category) {
                $sql = 'INSERT INTO `' . self::TABLE_CATEGORIES . '` (`ID`, `PARENT_ID`, `NAME`) VALUES ("' . $category['id'] . '","' . $category['parentId'] . '","' . addslashes($category['name']) . '")';
                $connection->query($sql);
            }
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }
    }


    /**
     * Точка входа в обновление/добавление разделов на сайте
     *
     * @throws \Exception
     */
    public function updateCategories(): void
    {
        $this->addLogMessage('Старт обновления разделов');

        $connection = Application::getConnection();

        try {
            // Все категории в таблице сопоставляем с существущими и проставляем Bitrix ID
            $sql = 'UPDATE `' . self::TABLE_CATEGORIES . '` as cat
               LEFT JOIN `b_iblock_section` AS sect ON sect.`XML_ID`=cat.`ID`
               SET cat.`BITRIX_ID` = sect.`ID`';
            $connection->query($sql);

            $sql = 'SELECT * FROM `' . self::TABLE_CATEGORIES . '` 
            WHERE `PARENT_ID` IS NULL
            OR `PARENT_ID` = "0"
            ORDER BY NAME';
            $categories = $connection->query($sql)->fetchAll();
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        foreach ($categories as $category) {
            $level = 0;
            $this->updateSection($category, $level);
        }

        $this->deactivateSections();

        // Ветки, у которых отсутствует верхний родитель и они не могут быть созданы
        try {
            $sql = 'SELECT COUNT(`ID`) AS NOPARENT_COUNT
               FROM `' . self::TABLE_CATEGORIES . '`
               WHERE `BITRIX_ID` IS NULL';
            $noparent = $connection->query($sql)->fetch();
            if ((int)$noparent['NOPARENT_COUNT'] > 0) {
                $this->addLogMessage('В XML-файле найдено ' . (int)$noparent['NOPARENT_COUNT'] . ' категорий, у которых отсутствует верхний родитель и которые не могут быть созданы');
            }
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        $this->addLogMessage('Обновление разделов завершено');
    }


    /**
     * Рекурсивное создание/обновление разделов. Создается/обновляется текущий раздел + его дочерние
     *
     * @param array $section массив с текущим разделом для создания/обновления
     * @param int $level отслеживание уровня для рекурсии
     * @throws \Exception
     */
    private function updateSection($section, &$level): void
    {
        $level++;
        if ($level > self::MAX_SECTION_DEPTH) {
            throw new \Exception('Превышена максимальная глубина вложенности секций: ' . self::MAX_SECTION_DEPTH . ' уровня');
        }

        $connection = Application::getConnection();

        try {
            $sql = 'SELECT `BITRIX_ID`
                 FROM `' . self::TABLE_CATEGORIES . '`
                 WHERE `ID`="' . $section['PARENT_ID'] . '"
                 LIMIT 1';
            $parent = $connection->query($sql)->fetch();
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        $fields = [
            'ACTIVE' => 'Y',
            'IBLOCK_SECTION_ID' => $parent['BITRIX_ID'],
            'IBLOCK_ID' => $this->iblockId,
            'NAME' => $section['NAME'],
            'CODE' => \CUtil::translit($section['NAME'], 'ru', []),
            'XML_ID' => $section['ID']
        ];

        $obSection = new \CIBlockSection();

        if ((int)$section['BITRIX_ID'] > 0) {
            $res = $obSection->Update((int)$section['BITRIX_ID'], $fields);
            if (!$res) {
                throw new \Exception('Ошибка обновления раздела XML_ID=[' . $section['ID'] . '] NAME=[' . $section['NAME'] . '] ERROR=[' . $obSection->LAST_ERROR . ']');
            }
        } else {
            $newId = $obSection->Add($fields);
            if ((int)$newId === 0) {
                throw new \Exception('Ошибка добавления раздела XML_ID=[' . $section['ID'] . '] NAME=[' . $section['NAME'] . '] ERROR=[' . $obSection->LAST_ERROR . ']');
            }

            $section['BITRIX_ID'] = $newId;

            try {
                $sql = 'UPDATE `' . self::TABLE_CATEGORIES . '` 
                        SET `BITRIX_ID` = "' . $section['BITRIX_ID'] . '"
                        WHERE `ID`="' . $section['ID'] . '"';
                $connection->query($sql);
            } catch (Exception $e) {
                throw new \Exception('При обновлении раздела XML_ID=[' . $section['ID'] . '] NAME=[' . $section['NAME'] . '] возникла ошибка запроса к БД [' . $e->getMessage() . ']');
            }
        }

        try {
            $sql = 'SELECT *
                    FROM `' . self::TABLE_CATEGORIES . '`
                    WHERE `PARENT_ID`="' . $section['ID'] . '"
                    ORDER BY `NAME`';
            $childs = $connection->query($sql)->fetchAll();
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        foreach ($childs as $child) {
            self::updateSection($child, $level);
        }

        $level--;
    }


    /**
     *  Деактивация разделов, которые не пришли в XML-файле
     *
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    private function deactivateSections(): void
    {
        $this->addLogMessage('Деактивация разделов...');
        $connection = Application::getConnection();

        try {
            $sql = 'SELECT sect.`ID`, sect.`NAME`
                    FROM `b_iblock_section` AS sect
                    LEFT JOIN `' . self::TABLE_CATEGORIES . '` AS cat ON cat.`BITRIX_ID`=sect.`ID`
                    WHERE sect.`IBLOCK_ID`=' . $this->iblockId . '
                    AND sect.`ACTIVE` = "Y"
                    AND cat.`BITRIX_ID` IS NULL';

            $sections = $connection->query($sql)->fetchAll();
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        $excludedSections = $this->getExcludedSections(true);

        $i = 0;
        foreach ($sections as $section) {
            if (in_array($section['ID'], $excludedSections, false)) {
                continue;
            }

            $obSection = new \CIBlockSection();
            $res = $obSection->Update((int)$section['ID'], ['ACTIVE' => 'N']);
            if (!$res) {
                throw new \Exception('Ошибка деактивации раздела ID=[' . $section['ID'] . '] NAME=[' . $section['NAME'] . '] ERROR=[' . $obSection->LAST_ERROR . ']');
            }

            $i++;
        }

        $this->addLogMessage('Деактивировано разделов: ' . $i);
    }

    /**
     * @param bool $includeElementsParents
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    private function getExcludedSections(bool $includeElementsParents = false): array
    {
        $connection = Application::getConnection();

        $iblockId = 1;

        $sql = 'SELECT sect.`ID`
                FROM `b_iblock_section` AS sect
                LEFT JOIN `b_uts_iblock_' . $iblockId . '_section` AS uf ON uf.`VALUE_ID` = sect.`ID`
                WHERE uf.`UF_IMPORT_EXCLUDE`="1"
                AND sect.`ACTIVE` = "Y"';

        $excludedSections = $connection->query($sql)->fetchAll();
        $excludedSections = array_column($excludedSections, 'ID');

        $sections = $excludedSections;

        if ($includeElementsParents) {
            $excludedElements = $this->getExcludedElements();
            if (!empty($excludedElements)) {
                $excludedElementsSections = array_column(
                    ElementTable::getList([
                        'select' => ['IBLOCK_SECTION_ID'],
                        'filter' => ['@ID' => $excludedElements],
                        'group' => ['IBLOCK_SECTION_ID'],
                    ])->fetchAll(),
                    'IBLOCK_SECTION_ID'
                );

                $sections += array_merge($sections, $excludedElementsSections);
            }
        }

        $sections = array_unique($sections);

        foreach ($sections as $section) {
            $sections += array_merge($sections, $this->getSectionParentsIds($section));
            $sections += array_merge($sections, $this->getSectionChildrenIds($section));
        }

        $sections = array_unique($sections);

        sort($sections);

        return $sections;
    }


    /**
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    private function getExcludedElements(): array
    {
        Loader::includeModule('iblock');

        $elements = [];

        $rsElements = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $this->iblockId, '!=PROPERTY_IMPORT_EXCLUDE' => false, 'ACTIVE' => 'Y', 'SECTION_GLOBAL_ACTIVE' => 'Y'],
            false,
            false,
            ['ID']
        );

        while ($element = $rsElements->Fetch()) {
            $elements[] = (int)$element['ID'];
        }

        return $elements;
    }


    /**
     * @param int $id
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    private function getSectionParentsIds(int $id): array
    {
        Loader::includeModule('iblock');

        $sectionData = SectionTable::getList([
            'select' => ['ID', 'IBLOCK_ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'],
            'filter' => ['=ID' => $id],
            'limit' => 1,
        ])->fetch();

        if (empty($sectionData)) {
            return [];
        }

        $sections = \CIBlockSection::GetNavChain($sectionData['IBLOCK_ID'], $sectionData['ID'], ['ID'], true);

        array_pop($sections);

        return array_column($sections, 'ID');
    }


    /**
     * @param int $id
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private function getSectionChildrenIds(int $id): array
    {
        $sectionData = SectionTable::getList(
            [
                'select' => ['ID', 'IBLOCK_ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'],
                'filter' => ['=ID' => $id],
                'limit' => 1,
            ]
        )->fetch();

        if (empty($sectionData)) {
            return [];
        }

        $sections = SectionTable::getList([
            'select' => ['ID'],
            'filter' => ['=IBLOCK_ID' => $sectionData['IBLOCK_ID'], '>=LEFT_MARGIN' => $sectionData['LEFT_MARGIN'], '<=RIGHT_MARGIN' => $sectionData['RIGHT_MARGIN'], '!=ID' => $sectionData['ID']],
        ])->fetchAll();

        return array_column($sections, 'ID');
    }


    /**
     * Чтение всех товаров из XML-файла и внесение их во временную таблицу для последующего обновления разделов инфоблока с товарами
     * Сбор списка всех параметров (свойств) товара для последующей актуализации IBLOCK_PROPERTY
     *
     * @throws \Exception
     */
    public function getOffersToTemporaryTable(): void
    {
        if (get_class($this->XML) !== 'SimpleXMLElement') {
            throw new \Exception('Прайс-лист не является объектом SimpleXMLElement');
        }

        $this->addLogMessage('Запись предложений во временную таблицу');
        $connection = Application::getConnection();

        $sql = 'SELECT ID, XML_ID FROM `b_iblock_element` 
               WHERE `IBLOCK_ID` = ' . $this->iblockId . '
               AND `XML_ID` IS NOT NULL';
        $elements = $connection->query($sql)->fetchAll();
        $elements = array_column($elements, 'ID', 'XML_ID');

        $sql = 'SELECT ID, BITRIX_ID FROM `' . self::TABLE_CATEGORIES . '`';
        $categories = $connection->query($sql)->fetchAll();
        $categories = array_column($categories, 'BITRIX_ID', 'ID');

        $this->paramsList = [];
        $arOffers = [];
        foreach ($this->XML->shop->offers->offer as $offer) {
            $attr = json_decode(json_encode($offer->attributes()), true);
            $arOffer = json_decode(json_encode($offer), true);

            $offerParams = [];
            foreach ($offer->param as $param) {
                $param = json_decode(json_encode($param), true);
                $paramName = trim($param['@attributes']['name']);
                $paramCode = \CUtil::translit($paramName, 'ru', ['change_case' => 'U']);
                if (preg_match('/^\d.+/u', $paramCode)) {
                    $paramCode = 'P' . $paramCode;
                } //Код свойства не может начинаться с цифры

                if (!in_array($paramCode, array_column($this->paramsList, 'code'), true)) {
                    $this->paramsList[] = [
                        'name' => $paramName,
                        'code' => $paramCode,
                    ];
                }

                $offerParams[] = [
                    'name' => $paramName,
                    'code' => $paramCode,
                    'value' => $param[0]
                ];
            }

            //$qty = floatval(preg_replace('/[^0-9]/ui','',$arOffer['quantity'])); // TODO выяснить, как нужно формировать доступное количество
            $qty = trim($arOffer['quantity']);

            if (is_array($arOffer['barcode'])) {
                $barcode = $arOffer['barcode'][0];
            } else {
                $barcode = $arOffer['barcode'];
            }

            if (is_array($arOffer['picture'])) {
                $arPictures = $arOffer['picture'];
            } else {
                $arPictures = [$arOffer['picture']];
            }

            $arOffers[] = [
                'id' => (int)$attr['@attributes']['id'],
                'bitrixId' => $elements[(int)$attr['@attributes']['id']] ? (int)$elements[(int)$attr['@attributes']['id']] : null,
                'available' => (stripos($attr['@attributes']['available'], 'true') !== false) ? "Y" : "N",
                'name' => trim($arOffer['name']),
                'url' => $arOffer['url'],
                'price' => (float)$arOffer['price'],
                'VAT' => (int)$arOffer['VAT'],
                'categoryId' => (int)$arOffer['categoryId'],
                'bitrixCategoryId' => !is_null($categories[(int)$arOffer['categoryId']]) ? (int)$categories[(int)$arOffer['categoryId']] : null,
                'pictures' => json_encode($arPictures),
                'delivery' => (stripos($arOffer['delivery'], 'true') !== false) ? "Y" : "N",
                'quantity' => $qty,
                'barcode' => $barcode,
                'vendor' => trim($arOffer['vendor']),
                'description' => trim($arOffer['description']),
                'country' => $arOffer['country_of_origin'],
                'params' => json_encode($offerParams)
            ];

            if (count($arOffers) % 5000 === 0) {
                $this->addLogMessage('Прочитано ' . count($arOffers) . ' предложений из XML-файла', 'DEBUG');
            }
        }
        $this->addLogMessage('Во временную таблицу будет записано ' . count($arOffers) . ' предложений', 'DEBUG');

        // В список свойств товара добавляем те, которые не в блоке param
        $this->paramsList[] = [
            'name' => 'Штрих-код',
            'code' => 'BARCODE',
        ];
        $this->paramsList[] = [
            'name' => 'Страна происхождения',
            'code' => 'COUNTRY_OF_ORIGIN',
        ];
        $this->paramsList[] = [
            'name' => 'Производитель',
            'code' => 'VENDOR',
        ];
        $this->paramsList[] = [
            'name' => 'Остаток',
            'code' => 'QUANTITY',
        ];

        try {
            $connection->query('DROP TABLE IF EXISTS `' . self::TABLE_OFFERS . '`');

            $this->addLogMessage('Начата запись предложений во временную таблицу', 'DEBUG');
            $sql = 'CREATE TABLE `' . self::TABLE_OFFERS . '` (
                        `ID` int(11) NOT NULL,
                        `BITRIX_ID` int(11) DEFAULT NULL,
                        `CATEGORY_ID` int(11) DEFAULT NULL,
                        `BITRIX_SECTION_ID` int(11) DEFAULT NULL,
                        `NAME` text COLLATE utf8_unicode_ci,
                        `URL` text COLLATE utf8_unicode_ci,
                        `QUANTITY` varchar(15) COLLATE utf8_unicode_ci DEFAULT "0",
                        `PRICE` double(10,2) DEFAULT "0",
                        `VAT` int(11) DEFAULT "20",
                        `BARCODE` varchar(16) COLLATE utf8_unicode_ci DEFAULT "",
                        `VENDOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT "",
                        `COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT "",
                        `AVAILABLE` varchar(2) COLLATE utf8_unicode_ci DEFAULT "",
                        `DELIVERY` varchar(2) COLLATE utf8_unicode_ci DEFAULT "",
                        `PICTURES` text COLLATE utf8_unicode_ci,
                        `DESCRIPTION` text COLLATE utf8_unicode_ci,
                        `PARAMS` text COLLATE utf8_unicode_ci,
                    PRIMARY KEY (`ID`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
            $connection->query($sql);

            foreach ($arOffers as $i => $offer) {
                if ($i % 5000 === 0) {
                    $this->addLogMessage('Во временную таблицу записано ' . $i . ' предложений', 'DEBUG');
                }

                $sql = 'INSERT IGNORE INTO `' . self::TABLE_OFFERS . '` 
                            (`ID`, `BITRIX_ID`, `CATEGORY_ID`, `BITRIX_SECTION_ID`, `NAME`, `URL`, `QUANTITY`, `PRICE`, `VAT`, `BARCODE`, `VENDOR`, `COUNTRY`, `AVAILABLE`, `DELIVERY`, `PICTURES`, `DESCRIPTION`, `PARAMS`) 
                        VALUES (
                            "' . $offer['id'] . '",
                            "' . $offer['bitrixId'] . '",
                            "' . $offer['categoryId'] . '",
                            "' . $offer['bitrixCategoryId'] . '",
                            "' . addslashes($offer['name']) . '",
                            "' . addslashes($offer['url']) . '",
                            "' . $offer['quantity'] . '",
                            "' . $offer['price'] . '",
                            "' . $offer['VAT'] . '",
                            "' . $offer['barcode'] . '",
                            "' . addslashes($offer['vendor']) . '",
                            "' . addslashes($offer['country']) . '",
                            "' . $offer['available'] . '",
                            "' . $offer['delivery'] . '",
                            "' . addslashes($offer['pictures']) . '",
                            "' . addslashes($offer['description']) . '",
                            "' . addslashes($offer['params']) . '"
                        )';
                $connection->query($sql);
            }
        } catch (Exception $e) {
            $this->addLogMessage('Ошибка запроса к БД [' . $e->getMessage() . ']', 'FATAL');
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }
    }


    /**
     * На основании списка всех свойств, которые присутствуют в XML-файле, создает таблицу komusprice_paramslist
     * Если свойство не найдено в ИБ, то оно создается
     * На выходе в таблице прописано соответствие PROPERTY_ID свойства инфоблока и свойства из XML-файла
     *
     * @throws \Exception
     *
     */
    public function updateIblockProperties(): void
    {
        if (count($this->paramsList) === 0) {
            throw new \Exception('В xml-файле не найдено ни одного свойства товаров');
        }

        $this->addLogMessage('Старт обновления свойств товаров');
        $this->addLogMessage('Найдено свойств товаров: ' . count($this->paramsList));

        $connection = Application::getConnection();

        try {
            $connection->query('DROP TABLE IF EXISTS `' . self::TABLE_PARAMSLIST . '`');

            $sql = 'CREATE TABLE `' . self::TABLE_PARAMSLIST . '` (
                        `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `PROPERTY_ID` int(10) unsigned DEFAULT NULL,
                        `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT "UNKNOWN_CODE",
                        `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT "NONAME",
                    PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

            $connection->query($sql);

            foreach ($this->paramsList as $param) {
                $sql = 'INSERT INTO `' . self::TABLE_PARAMSLIST . '` (`CODE`, `NAME`) VALUES ("' . addslashes($param['code']) . '", "' . addslashes($param['name']) . '")';
                $connection->query($sql);
            }

            // Расставляем в таблице ID уже существующих свойств
            $sql = 'UPDATE `' . self::TABLE_PARAMSLIST . '` as list
                    LEFT JOIN `b_iblock_property` AS prop ON prop.`CODE`=list.`CODE`
                    SET list.`PROPERTY_ID` = prop.`ID`
                    WHERE prop.`IBLOCK_ID`="' . $this->iblockId . '"';
            $connection->query($sql);

            $sql = 'SELECT `ID`,`CODE`,`NAME` FROM `' . self::TABLE_PARAMSLIST . '` 
                    WHERE `PROPERTY_ID` IS NULL
                    ORDER BY `CODE`';
            $newProps = $connection->query($sql)->fetchAll();

            $i = 0;
            foreach ($newProps as $prop) {
                $fields = [
                    'NAME' => $prop['NAME'],
                    'ACTIVE' => 'Y',
                    'MULTIPLE' => 'N',
                    'CODE' => $prop['CODE'],
                    'PROPERTY_TYPE' => 'S',
                    'IBLOCK_ID' => $this->iblockId,
                    'WITH_DESCRIPTION' => 'N',
                ];

                $ibp = new \CIBlockProperty();
                $id = $ibp->Add($fields);

                if ($id === false) {
                    throw new \Exception('Ошибка создания нового свойства товаров [' . $ibp->LAST_ERROR . ']');
                }

                $i++;

                $sql = 'UPDATE `' . self::TABLE_PARAMSLIST . '` 
                        SET `PROPERTY_ID`="' . $id . '" 
                        WHERE `ID`="' . $prop['ID'] . '"';
                $connection->query($sql);
            }

            $this->addLogMessage('Добавлено в ИБ новых свойств товаров: ' . $i);

            // Проверка свойства с фотогалереей
            $photogalleryProperty = PropertyTable::getList([
                'select' => ['ID'],
                'filter' => ['=IBLOCK_ID' => $this->iblockId, '=CODE' => self::PHOTOGALLERY_CODE],
                'limit' => 1,
            ])->fetch();

            if ((int)$photogalleryProperty['ID'] === 0) {
                $fields = [
                    'NAME' => self::PHOTOGALLERY_NAME,
                    'ACTIVE' => 'Y',
                    'MULTIPLE' => 'Y',
                    'SORT' => 10,
                    'CODE' => self::PHOTOGALLERY_CODE,
                    'PROPERTY_TYPE' => 'F',
                    'FILE_TYPE' => 'jpg, jpeg, png, gif, bmp',
                    'IBLOCK_ID' => $this->iblockId,
                    'WITH_DESCRIPTION' => 'N',
                ];

                $ibp = new \CIBlockProperty();
                $id = $ibp->Add($fields);

                if ($id === false) {
                    throw new \Exception('Ошибка создания свойства Фотогалерея [' . $ibp->LAST_ERROR . ']');
                }

                $this->photogalleryPropertyId = $id;
            } else {
                $this->photogalleryPropertyId = (int)$photogalleryProperty['ID'];
            }
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }
    }


    /**
     * Обновление/добавление товаров
     * Товары с неустановленным ID раздела игнорируются
     *
     * @throws ArgumentException
     * @throws ObjectNotFoundException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws \Exception
     */
    public function updateOffers(): void
    {
        $this->addLogMessage('Старт обновления товаров');

        $connection = Application::getConnection();

        try {
            $this->addLogMessage('Получение списка предложений из ' . self::TABLE_OFFERS, 'DEBUG');

            $sql = 'SELECT * FROM `' . self::TABLE_OFFERS . '` 
                    WHERE `BITRIX_SECTION_ID` IS NOT NULL
                    OR `BITRIX_SECTION_ID` <> "0"
                    ORDER BY `NAME`';
            $offers = $connection->query($sql)->fetchAll();

            $this->addLogMessage('Получение списка свойств из ' . self::TABLE_PARAMSLIST, 'DEBUG');

            $sql = 'SELECT `PROPERTY_ID` AS ID, `CODE` FROM `' . self::TABLE_PARAMSLIST . '` 
                    WHERE `PROPERTY_ID` IS NOT NULL
                    OR `PROPERTY_ID` <> "0"
                    ORDER BY `CODE`';
            $params = $connection->query($sql)->fetchAll();
        } catch (Exception $e) {
            $this->addLogMessage('Ошибка запроса к БД [' . $e->getMessage() . ']', 'FATAL');
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        // Массив со свойствами инфоблока
        $this->addLogMessage('Формирование списка свойств инфоблока', 'DEBUG');
        $propertiesList = [];
        foreach ($params as $param) {
            $this->addLogMessage($param['CODE'], 'DEBUG');
            $propertiesList[$param['CODE']] = $param['ID'];
        }

        $errorUpdate = []; // Список товаров, при обновлении которых произошла ошибка
        $errorAdd = []; // Список товаров, при добавлении которых произошла ошибка

        $itemsUpdated = 0;
        $itemsAdded = 0;
        $i = 0;

        foreach ($offers as $offer) {
            $this->addLogMessage('Начало обработки товара n=' . ($i+1) . ' с XML_ID=' . $offer['ID'], 'DEBUG');

            $fieldsItem = [
                'NAME' => $offer ['NAME'],
                'ACTIVE' => 'Y',
                'CODE' => \CUtil::translit($offer['NAME'], 'ru', []) . '_' . $offer['ID'],
                'IBLOCK_ID' => $this->iblockId,
                'IBLOCK_SECTION_ID' => $offer['BITRIX_SECTION_ID'],
                'DETAIL_TEXT' => $offer['DESCRIPTION'],
                'XML_ID' => $offer['ID'],
            ];

            $obElement = new \CIBlockElement();

            if ((int)$offer['BITRIX_ID'] > 0) {
                $res = $obElement->Update($offer['BITRIX_ID'], $fieldsItem);
                if (!$res) {
                    $errorUpdate[] = 'XML_ID=[' . $offer['ID'] . '] BITRIX_ID=[' . $offer['BITRIX_ID'] . '] NAME=[' . $offer['NAME'] . '] ERROR=[' . $obElement->LAST_ERROR . ']';
                    continue;
                }
                $itemsUpdated++;
            } else {
                $newId = $obElement->Add($fieldsItem);
                if ($newId === false) {
                    $errorAdd[] = 'XML_ID=[' . $offer['ID'] . '] NAME=[' . $offer['NAME'] . '] ERROR=[' . $obElement->LAST_ERROR . ']';
                    continue;
                }
                $itemsAdded++;

                try {
                    $sql = 'UPDATE `' . self::TABLE_OFFERS . '` 
                     SET `BITRIX_ID` = "' . $newId . '"
                     WHERE `ID`="' . $offer['ID'] . '"';
                    $connection->query($sql);
                } catch (Exception $e) {
                    // TODO Ошибка не критическая, нужен какой-то обработчик
                }

                $offer['BITRIX_ID'] = $newId;
            }

            // Обновление свойств товара

            $params = json_decode($offer['PARAMS'], true);

            $propertyValues = [];
            foreach ($params as $param) {
                if ((int)$propertiesList[$param['code']] > 1) {
                    $propertyValues[$propertiesList[$param['code']]] = $param['value'];
                }
            }

            $propertyValues['BARCODE'] = $offer['BARCODE'];
            $propertyValues['COUNTRY_OF_ORIGIN'] = $offer['COUNTRY'];
            $propertyValues['VENDOR'] = $offer['VENDOR'];
            $propertyValues['QUANTITY'] = $offer['QUANTITY'];
            $propertyValues['ARTICLE'] = $offer['ID'];

            \CIBlockElement::SetPropertyValuesEx($offer['BITRIX_ID'], $this->iblockId, $propertyValues);

            // Обновление торгового каталога

            if ((int)$offer['VAT'] === 0) {
                $vat = 1;
            } elseif ((int)$offer['VAT'] === 20) {
                $vat = 2;
            } else {
                $vat = 2;
            }

            $unit = 5; // В xml-файле у товаров нет поля с единицей измерения, все товары считаем штучными

            $fieldsCatalog = [
                'ID' => $offer['BITRIX_ID'],
                'QUANTITY' => 1,
                'MEASURE' => $unit,
                'CAN_BUY_ZERO' => 'Y',
                'AVAILABLE' => 'Y',
                'VAT_ID' => $vat,
                'VAT_INCLUDED' => 'Y'
            ];

            $fieldsPrice = [
                'PRODUCT_ID' => $offer['BITRIX_ID'],
                'PRICE' => $offer['PRICE'],
                'CATALOG_GROUP_ID' => 1, // базовая цена
                'CURRENCY' => 'RUB',
            ];

            $catalog = Product::getCacheItem($fieldsCatalog['ID'], true);
            try {
                if (!empty($catalog)) {
                    Product::update($fieldsCatalog['ID'], $fieldsCatalog);
                } else {
                    Product::add($fieldsCatalog);
                }
            } catch (Exception $e) {
                // TODO возможно, это тоже нужно в лог писать
            }

            $price = Price::getList([
                'filter' => ['PRODUCT_ID' => $fieldsPrice['PRODUCT_ID']],
                'limit' => 1
            ])->fetch();

            try {
                if (!empty($price)) {
                    Price::update($price['ID'], $fieldsPrice);
                } else {
                    Price::add($fieldsPrice);
                }
            } catch (Exception $e) {
                // TODO возможно, это тоже нужно в лог писать
            }

            $i++;
            if ($i % 5000 === 0) {
                $this->addLogMessage('Обработано товаров: ' . $i);
            }

            $this->addLogMessage('Обработан товар n=' . $i . ' с XML_ID=' . $offer['ID'], 'DEBUG');
            $this->addLogMessage('Обработано товаров: ' . $i, 'DEBUG');
        }

        $this->addLogMessage('Добавлено товаров: ' . $itemsAdded);
        $this->addLogMessage('Обновлено товаров: ' . $itemsUpdated);

        $this->deactivateOffers();

        $this->addLogMessage('Обновление товаров завершено');
    }


    /**
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     */
    private function deactivateOffers(): void
    {
        $this->addLogMessage('Деактивация товаров...');
        $connection = Application::getConnection();

        $sql = 'SELECT elem.`ID`, elem.`IBLOCK_SECTION_ID`
                FROM `b_iblock_element` AS elem
                LEFT JOIN `' . self::TABLE_OFFERS . '` AS offer ON offer.`BITRIX_ID`=elem.`ID`
                WHERE elem.`IBLOCK_ID`=' . $this->iblockId . '
                AND elem.`ACTIVE` = "Y"
                AND offer.`BITRIX_ID` IS NULL';
        $elements = $connection->query($sql)->fetchAll();

        $excludedElements = $this->getExcludedElements();
        $excludedSections = $this->getExcludedSections();

        $i = 0;
        foreach ($elements as $element) {
            if (in_array($element['ID'], $excludedElements, false) || in_array($element['IBLOCK_SECTION_ID'], $excludedSections, false)) {
                continue;
            }

            $obElement = new \CIBlockElement;
            $res = $obElement->Update((int)$element['ID'], ['ACTIVE' => 'N']);
            if (!$res) {
                throw new \Exception('Ошибка деактивации товара ID=[' . $element['ID'] . '] ERROR=[' . $obElement->LAST_ERROR . ']');
            }

            $i++;
        }

        $this->addLogMessage('Деактивировано товаров: ' . $i);
    }


    /**
     * @param bool $compareMD5
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     */
    public function updatePictures(bool $compareMD5 = false): void
    {
        Loader::includeModule('iblock');

        $this->addLogMessage('Старт обновления изображений');

        $propsClass = Base::compileEntity(
            'PROPS',
            [
                'ID' => ['data_type' => 'integer'],
                'IBLOCK_ELEMENT_ID' => ['data_type' => 'integer'],
                'IBLOCK_PROPERTY_ID' => ['data_type' => 'integer'],
                'VALUE' => ['data_type' => 'string']
            ],
            [
                'table_name' => 'b_iblock_element_property'
            ]
        )->getDataClass();

        $connection = Application::getConnection();

        try {
            $sql = 'SELECT `BITRIX_ID` ,`PICTURES`
                    FROM `' . self::TABLE_OFFERS . '`
                    WHERE `BITRIX_ID` IS NOT NULL
                    OR `BITRIX_ID` <> 0
                    ORDER BY `ID`';
        } catch (Exception $e) {
            throw new \Exception('Ошибка запроса к БД [' . $e->getMessage() . ']');
        }

        $offers = $connection->query($sql)->fetchAll();

        $i = 0;

        foreach ($offers as $offer) {
            $rawFilesList = json_decode($offer['PICTURES'], true);

            $offerPicturesList = array_filter($rawFilesList, static function ($item) {
                if (!preg_match('/^http(s?):\/\/(\S+)\.(jpeg|jpg|png)$/ui', $item)) {
                    return false;
                }

                return true;
            });

            if (count($offerPicturesList) === 0) {
                continue;
            }

            $this->multiplyDownload($offerPicturesList);

            array_walk($offerPicturesList, static function(&$item) {
                $item = basename($item);
            });

            natsort($offerPicturesList);

            // Обработка первого изображения
            // Проверяем, скачалось ли изображение
            if (file_exists($this->imagesTmpDir . '/' . $offerPicturesList[0])) {
                $elementDetailPicture = FileTable::getList([
                    'select' => ['ID', 'SUBDIR', 'FILE_NAME', 'ORIGINAL_NAME', 'ELEMENT.ID',],
                    'filter' => ['ELEMENT.ID' => $offer['BITRIX_ID']],
                    'limit' => 1,
                    'runtime' => [
                        new ReferenceField(
                            'ELEMENT',
                            ElementTable::class,
                            Join::on('this.ID', 'ref.DETAIL_PICTURE')
                        )
                    ],
                ])->fetch();

                if ((int)$elementDetailPicture['ID'] === 0) {
                    // У товара нет детального изображения
                    $this->updateDetailPicture((int)$offer['BITRIX_ID'], $offerPicturesList[0]);
                } else {
                    // У товара есть детальное изображение
                    $h1 = md5_file(
                        $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $elementDetailPicture['SUBDIR'] . '/' . $elementDetailPicture['FILE_NAME']
                    );
                    $h2 = md5_file($this->imagesTmpDir . '/' . $offerPicturesList[0]);

                    if (($h1 !== $h2) && $this->updateDetailPicture((int)$offer['BITRIX_ID'], $offerPicturesList[0])) {
                        \CFile::Delete($elementDetailPicture['ID']);
                    }
                }

                @unlink($this->imagesTmpDir . '/' . $offerPicturesList[0]);
            }
            unset ($offerPicturesList[0]);

            // Обработка галереи
            $newPhotogalleryList = []; // Массив для обновления свойства PROPERTY_PHOTOGALLERY

            // Получение списка загруженных ранее изображений
            $existingFiles = FileTable::getList([
                'select' => [
                    'ID',
                    'SUBDIR',
                    'FILE_NAME',
                    'ORIGINAL_NAME',
                    'PROPS_GALLERY.IBLOCK_ELEMENT_ID',
                    'PROPS_GALLERY.IBLOCK_PROPERTY_ID',
                    'PROP_ID' => 'PROPS_GALLERY.ID'
                ],
                'filter' => [
                    '=PROPS_GALLERY.IBLOCK_PROPERTY_ID' => $this->photogalleryPropertyId,
                    '=PROPS_GALLERY.IBLOCK_ELEMENT_ID' => $offer['BITRIX_ID']
                ],
                'runtime' => [
                        'PROPS_GALLERY' => [
                            'data_type' => $propsClass,
                            'reference' => [
                                '=this.ID' => 'ref.VALUE'
                            ],
                            'join_type' => 'inner'
                        ]
                ],
            ])->fetchAll();

            foreach ($existingFiles as $existingFile) {
                $a = array_search($existingFile['FILE_NAME'], $offerPicturesList, true);

                if ($a !== false) { // Изображение с таким именем уже загружено ранее

                    // Если удаленное изображение не скачалось, удаляем его из массива и пропускаем обработку
                    if (!file_exists($this->imagesTmpDir . '/' . $offerPicturesList[$a])) {
                        $newPhotogalleryList[$existingFile['ID']] = $existingFile['FILE_NAME'];
                        unset ($offerPicturesList[$a]);
                        continue;
                    }

                    $h1 = md5_file(
                        $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $existingFile['SUBDIR'] . '/' . $existingFile['FILE_NAME']
                    );
                    $h2 = md5_file($this->imagesTmpDir . '/' . $offerPicturesList[$a]);

                    if ($h1 !== $h2) {
                        $arFile = \CFile::MakeFileArray($this->imagesTmpDir . '/' . $offerPicturesList[$a]);
                        $arFile['MODULE_ID'] = 'iblock';
                        $newGalleryPictureId = (int)\CFile::SaveFile($arFile, 'iblock');

                        if ($newGalleryPictureId > 0) {
                            \CFile::Delete($existingFile['ID']);

                            $newPhotogalleryList[$newGalleryPictureId] = $existingFile['FILE_NAME'];

                        } else {
                            $newPhotogalleryList[$existingFile['ID']] = $existingFile['FILE_NAME']; // Новая картинка по какой-то причине не загрузилась, оставляем старую
                        }
                    } else {
                        $newPhotogalleryList[$existingFile['ID']] = $existingFile['FILE_NAME'];  // Хэш картинок совпадает, оставляем старую
                    }

                    @unlink($this->imagesTmpDir . '/' . $offerPicturesList[$a]);
                    unset ($offerPicturesList[$a]);
                } else {
                    // Изображения нет в xml-файле, удаляем
                    \CFile::Delete($existingFile['ID']);
                }
            }

            // В массиве $offerPicturesList остались только новые картинки, которые ранее не были загружены в фотогалерею товара
            foreach ($offerPicturesList as $newPicture) {
                // Пропускаем, если удаленное изображение не скачалось
                if (!file_exists($this->imagesTmpDir . '/' . $newPicture)) {
                    continue;
                }

                $arFile = \CFile::MakeFileArray($this->imagesTmpDir . '/' . $newPicture);
                $arFile['MODULE_ID'] = 'iblock';
                $newGalleryPictureId = (int)\CFile::SaveFile($arFile, 'iblock');

                if ($newGalleryPictureId > 0) {
                    $newPhotogalleryList[$newGalleryPictureId] = $newPicture;
                }

                @unlink($this->imagesTmpDir . '/' . $newPicture);
            }

            // В массиве $newPhotogalleryList сейчас актуальный список уже загруженных и обработанных картинок

            asort($newPhotogalleryList);

            try {
                $sql = 'DELETE FROM `b_iblock_element_property`
                        WHERE `IBLOCK_ELEMENT_ID` = "' . $offer['BITRIX_ID'] . '"
                        AND `IBLOCK_PROPERTY_ID` = "' . $this->photogalleryPropertyId . '"';
                $connection->query($sql);
            } catch (Exception $e) {
                continue; // При ошибке галерею не обновляем
            }

            $propFilesList = [];
            foreach ($newPhotogalleryList as $id => $fileName) {
                $propFilesList[] = ['VALUE' => $id];
            }
            \CIBlockElement::SetPropertyValueCode($offer['BITRIX_ID'], $this->photogalleryPropertyId, $propFilesList);

            $i++;
            if ($i % 5000 === 0) {
                $this->addLogMessage('Товаров с загруженными картинками: ' . $i);
            }

            $this->addLogMessage('Обработаны изображения товара n=' . $i . ' с ID=' . $offer['BITRIX_ID'], 'DEBUG');
        }

        $this->addLogMessage('Обработка изображений завершена');
    }


    /**
     * Замена / загрузка детальной картинки для товара
     *
     * @param int $itemId ID товара в битриксе
     * @param string $fileName файл с картинкой, которая должна стать детальной
     * @return bool
     */
    private function updateDetailPicture(int $itemId, string $fileName): bool
    {
        $connection = Application::getConnection();

        $arFile = \CFile::MakeFileArray($this->imagesTmpDir . '/' . $fileName);
        $arFile['MODULE_ID'] = 'iblock';
        $newDetailPictureId = (int)\CFile::SaveFile($arFile, 'iblock');

        if ($newDetailPictureId > 0) {
            try {
                $sql = 'UPDATE `b_iblock_element`
                        SET `DETAIL_PICTURE` = "' . $newDetailPictureId . '"
                        WHERE `ID` = "' . $itemId . '"';
                $connection->query($sql);
            } catch (Exception $e) {
                return false;
            }
        }

        return ($newDetailPictureId > 0);
    }


    /**
     * Очистка сущестувющего инфоблока от старых товаров и свойств
     */
    public function clearExistingIblock(): void
    {
        $elements = ElementTable::getList([
            'select' => ['ID'],
            'filter' => ['=IBLOCK_ID' => $this->iblockId]
        ])->fetchAll();

        foreach ($elements as $element) {
            \CIBlockElement::Delete($element['ID']);
            \CCatalogProduct::Delete($element['ID']);
        }

        $properties = PropertyTable::getList([
            'select' => ['ID'],
            'filter' => ['=IBLOCK_ID' => $this->iblockId]
        ])->fetchAll();

        foreach ($properties as $property) {
            \CIBlockProperty::Delete($property['ID']);
        }

        $sections = SectionTable::getList([
            'select' => ['ID'],
            'filter' => ['=IBLOCK_ID' => $this->iblockId, '=IBLOCK_SECTION_ID' => null]
        ])->fetchAll();

        foreach ($sections as $section) {
            \CIBlockSection::Delete($section['ID']);
        }
    }


    /**
     * @param array $urls
     * @param int $try
     */
    private function multiplyDownload(array $urls, int $try = 1): void
    {
        $maxTries = 5;
        $packetSize = 10;
        $connectTimeout = 10;

        $urlsPackets = array_chunk($urls, $packetSize, true);

        foreach ($urlsPackets as $p => $urlsPacket) {
            $urlsToRetry = [];
            $connection = [];

            $mh = curl_multi_init();

            foreach ($urlsPacket as $i => $url) {
                $localFile = $this->imagesTmpDir . '/' . basename($url);

                $connection[$i] = curl_init($url);

                $fp[$i] = fopen($localFile, 'wb');

                curl_setopt($connection[$i], CURLOPT_FILE, $fp[$i]);
                curl_setopt($connection[$i], CURLOPT_HEADER, 0);
                curl_setopt($connection[$i], CURLOPT_CONNECTTIMEOUT, $connectTimeout);
                curl_multi_add_handle($mh, $connection[$i]);
            }

            do {
                $n = curl_multi_exec($mh, $active);
            } while ($active);

            foreach ($urlsPacket as $i => $url) {
                $httpCode = (int)curl_getinfo($connection[$i], CURLINFO_HTTP_CODE);
                if ($httpCode >= 500) {
                    $urlsToRetry[] = $url;
                }

                curl_multi_remove_handle($mh, $connection[$i]);
                curl_close($connection[$i]);

                fclose($fp[$i]);

                $active = null;

                $localFile = $this->imagesTmpDir . '/' . basename($url);
                if ($httpCode >= 400) {
                    $this->addLogMessage('Ошибка загрузки изображения ' . $httpCode . ' ' . $url, 'DEBUG');
                    unlink($localFile);
                    continue;
                }
            }
            curl_multi_close($mh);

            if ($try <= $maxTries && !empty($urlsToRetry)) {
                $try++;
                $this->multiplyDownload($urlsToRetry, $try);
            }
            else {
                continue;
            }
        }
    }

}
